// modified from storyboard BSD clause 3 licensed
// https://github.com/ilikerobots/storyboard
// BSD clasue 2 copyright 2019 Fredrick Grott


import 'package:flutter/material.dart';
import 'package:recase/recase.dart';

/// A material app intended to display a Pageboard.
///
/// ## Sample code
///
/// ```dart
/// runApp(new PageboardApp([
///     new MyFancyWidgetStory(),
///     new MyBasicWidgetStory(),
/// ]));
/// ```
class PageboardApp extends MaterialApp{
  PageboardApp(List<Story> stories)
      : assert(stories != null),
  super(home: Pageboard(stories));
}

/// A Pageboard is a widget displaying a collection of [Story] widgets.
///
/// The Pageboard is a simple [Scaffold] widget that displays its [Story]
/// widgets in vertical [ListView].
///
/// See also [PageboardApp] for a simple Material app consisting of a single
/// Pageboard.
///
/// ## Sample code
///
/// ```dart
/// runApp(
///     new MaterialApp(
///         home: new Pageboard([
///             new MyFancyWidgetStory(),
///             new MyBasicWidgetStory(),
///         ])));
/// ```
class Pageboard extends StatelessWidget {
  final String _kPageBoardTitle = 'Pageboard';

  // ignore: sort_constructors_first, prefer_const_constructors_in_immutables
  Pageboard(this.stories)
      : assert(stories != null),
        super();

  final List<Story> stories;
  // I need to wrap lisrview according to https://stackoverflow.com/questions/50688970/cant-add-a-listview-in-flutter
  // by using expaded like this
  // https://medium.com/@DakshHub/flutter-displaying-dynamic-contents-using-listview-builder-f2cedb1a19fb
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: Text(_kPageBoardTitle)),
        body: ListView.builder(
          itemBuilder: (BuildContext context, int index) => stories[index],
          itemCount: stories.length,
        ));
  }
}

// A Story widget is intended as a single "page" of a [Pageboard].  It is
/// intended that authors write their own concrete implementations of Stories
/// to include in a [Pageboard].
///
/// A story consists of one or more Widgets.  Each Story is rendered as either
/// a [ExpansionTile] or, in the case when there exists only a single
/// fullscreen widget, as [ListTile].
///
/// The story's Widget children are arranged as a series of [Row]s within an
/// ExpansionTile, or if the widget is full screen, is displayed by navigating
/// to a new route.
abstract class Story extends StatelessWidget {
  const Story({Key key}) : super(key: key);

  List<Widget> get storyContent;

  String get title => ReCase(runtimeType.toString()).titleCase;

  bool get isFullScreen => false;

  Widget _widgetListItem(Widget w) =>
      // ignore: always_specify_types
      Row(mainAxisAlignment: MainAxisAlignment.center, children: [
        Container(
            padding: const EdgeInsets.symmetric(vertical: 8.0), child: w)
      ]);

  Widget _widgetTileLauncher(Widget w, String title, BuildContext context) =>
      ListTile(
          leading: const Icon(Icons.launch),
          title: Text(title),
          onTap: () {
            Navigator.push(context,
                // ignore: prefer_void_to_null
                MaterialPageRoute<Null>(builder: (BuildContext context) {
                  return w;
                }));
          });

  @override
  Widget build(BuildContext context) {
    if (!isFullScreen) {
      return ExpansionTile(
        leading: const Icon(Icons.list),
        key: PageStorageKey<Story>(this),
        title: Text(title),
        children: storyContent.map(_widgetListItem).toList(),
      );
    } else {
      if (storyContent.length == 1) {
        return _widgetTileLauncher(storyContent[0], title, context);
      } else {
        return ExpansionTile(
          leading: const Icon(Icons.fullscreen),
          key: PageStorageKey<Story>(this),
          title: Text(title),
          children: storyContent
              .map((Widget w) => _widgetTileLauncher(w, title, context))
              .toList(),
        );
      }
    }
  }
}

/// A convenience abstract class for implementing a full screen [Story].
abstract class FullScreenStory extends Story {
  @override
  bool get isFullScreen => true;
}