import 'package:flutter/material.dart';
import 'package:pageboard_roadtest_app/app_config.dart';
import 'package:pageboard_roadtest_app/my_home_page.dart';

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final AppConfig config = AppConfig.of(context);

    return MaterialApp(
      title: config.appName,
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(),
    );
  }
}