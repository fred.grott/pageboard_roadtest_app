class Task {
  final int id;
  final String title;
  final String description;
  bool done;
  int pomCount;

  // ignore: sort_constructors_first
  Task(this.id, this.title, this.description, this.done, [this.pomCount = 0]);

  // ignore: sort_constructors_first
  factory Task.fromMap(Map<String, dynamic> json) => Task(
      json['id'],
      json['title'],
      json['description'],
      // ignore: avoid_bool_literals_in_conditional_expressions
      json['done'] == 1 ? true : false,
      json['pomCount']);
}
