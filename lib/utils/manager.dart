import 'package:pageboard_roadtest_app/utils/database.dart';
import 'package:pageboard_roadtest_app/models/task.dart';

class Manager {
  Future<List<Task>> tasksData;

  dynamic addNewTask(Task task) async {
    await DatabaseUtil.db.insert(task);
  }

  dynamic updateTask(Task task) async {
    await DatabaseUtil.db.update(task);
  }

  dynamic removeTask(Task task) async {
    await DatabaseUtil.db.remove(task);
  }

  dynamic loadAllTasks() {
    tasksData = DatabaseUtil.db.getAll();
  }
}
