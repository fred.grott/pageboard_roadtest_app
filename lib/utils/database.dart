import 'dart:async';
import 'dart:io';

import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';
import 'package:pageboard_roadtest_app/models/task.dart';

class DatabaseUtil {
  DatabaseUtil._();

  static final DatabaseUtil db = DatabaseUtil._();
  static Database _database;

  Future<Database> get database async {
    if (_database != null) {
      return _database;
    }

    _database = await init();
    return _database;
  }

  dynamic init() async {
    final Directory documentDirectory = await getApplicationDocumentsDirectory();
    final String path = join(documentDirectory.path, 'tasks.db');

    // ignore: always_specify_types
    return await openDatabase(path, version: 1, onOpen: (db) {},
        onCreate: (Database db, int version) async {
          await db.execute('CREATE TABLE Task ('
              'id INTEGER PRIMARY KEY,'
              'title TEXT,'
              'description TEXT,'
              'done INTEGER,'
              'pomCount INTEGER'
              ')');
        });
  }

  dynamic insert(Task task) async {
    final Database db = await database;

    // ignore: always_specify_types
    final table = await db.rawQuery('SELECT MAX(id)+1 as id FROM Task');
    final dynamic id = table.first['id'];

    final int raw = await db.rawInsert(
        'INSERT Into Task (id, title, description, done, pomCount) VALUES (?,?,?,?,?)',
        <dynamic>[id, task.title, task.description, task.done ? 1 : 0, task.pomCount]);

    print('Saved: $raw');
    return raw;
  }

  dynamic update(Task task) async {
    final Database db = await database;

    final int raw = await db.rawUpdate(
        'UPDATE Task SET title = ?, description = ?, done = ?, pomCount = ? WHERE id = ?',
        <dynamic>[
          task.title,
          task.description,
          task.done ? 1 : 0,
          task.pomCount,
          task.id
        ]);

    print('Updated');
    return raw;
  }

  dynamic remove(Task task) async {
    final Database db = await database;

    final int raw = await db.rawUpdate('DELETE FROM Task WHERE id = ?', <dynamic>[task.id]);

    print('Removed');
    return raw;
  }

  Future<List<Task>> getAll() async {
    final Database db = await database;
    // ignore: always_specify_types
    final query = await db.query('Task');

    final List<Task> tasks =
    // ignore: always_specify_types
    query.isNotEmpty ? query.map((t) => Task.fromMap(t)).toList() : [];

    return tasks;
  }
}
