import 'package:flutter/material.dart';
import 'package:pageboard_roadtest_app/app_config.dart';
import 'package:pageboard_roadtest_app/my_app.dart';

void main() {
  final AppConfig configuredApp = AppConfig(
    appName: 'Build flavors DEV',
    flavorName: 'development',
    apiBaseUrl: 'https://dev-api.example.com/',
    child: MyApp(),
  );

  // rest of app config here have to repeat oh well

  runApp(configuredApp);
}