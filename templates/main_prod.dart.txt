import 'package:flutter/material.dart';
import 'package:pageboard_roadtest_app/app_config.dart';
import 'package:pageboard_roadtest_app/my_app.dart';

void main() {
  final AppConfig configuredApp = AppConfig(
    appName: 'Build flavors',
    flavorName: 'production',
    apiBaseUrl: 'https://api.example.com/',
    child: MyApp(),
  );

  runApp(configuredApp);
}